namespace TFMGame
{
    using GoogleARCore;
    using GoogleARCore.CrossPlatform;
    using UnityEngine;
    using UnityEngine.Networking;

    /// <summary>
    /// A Controller for the Board object that handles hosting and resolving the Cloud Anchor.
    /// Controlador para el objeto principal de la partida que define nuestro sistema de coordenadas de referencia.
    /// </summary>
#pragma warning disable 618
    public class BoardAnchorController : NetworkBehaviour
#pragma warning restore 618
    {
        /// <summary>
        /// The Cloud Anchor ID that will be used to host and resolve the Cloud Anchor. This
        /// variable will be synchronized over all clients.
        /// </summary>
#pragma warning disable 618
        [SyncVar(hook = "_OnChangeId")]
#pragma warning restore 618
        private string m_CloudAnchorId = string.Empty;

        /// <summary>
        /// Indicates whether this script is running in the Host.
        /// </summary>
        private bool m_IsHost;

        /// <summary>
        /// Indicates whether an attempt to resolve the Cloud Anchor should be made.
        /// </summary>
        private bool m_ShouldResolve;

        /// <summary>
        /// The Cloud Anchor Scene controller.
        /// </summary>
        private SceneController m_SceneController;

        /// <summary>
        /// The Unity Start() method.
        /// </summary>
        public void Start()
        {
            m_SceneController =
                GameObject.Find("CloudAnchorSceneController")
                    .GetComponent<SceneController>();
        }

        /// <summary>
        /// The Unity OnStartClient() method.
        /// </summary>
        public override void OnStartClient()
        {
            if (m_CloudAnchorId != string.Empty)
            {
                m_ShouldResolve = true;
            }
        }

        /// <summary>
        /// The Unity Update() method.
        /// </summary>
        public void Update()
        {
            if (m_ShouldResolve)
            {
                _ResolveAnchorFromId(m_CloudAnchorId);
            }
        }

        /// <summary>
        /// Command run on the server to set the Cloud Anchor Id.
        /// </summary>
        /// <param name="cloudAnchorId">The new Cloud Anchor Id.</param>
#pragma warning disable 618
        [Command]
#pragma warning restore 618
        public void CmdSetCloudAnchorId(string cloudAnchorId)
        {
            m_CloudAnchorId = cloudAnchorId;
        }

        /// <summary>
        /// Gets the Cloud Anchor Id.
        /// </summary>
        /// <returns>The Cloud Anchor Id.</returns>
        public string GetCloudAnchorId()
        {
            return m_CloudAnchorId;
        }

        /// <summary>
        /// Hosts the user placed cloud anchor and associates the resulting Id with this object.
        /// </summary>
        /// <param name="lastPlacedAnchor">The last placed anchor.</param>
        public void HostLastPlacedAnchor(Component lastPlacedAnchor)
        {
            m_IsHost = true;

            var anchor = (Anchor) lastPlacedAnchor;
            XPSession.CreateCloudAnchor(anchor).ThenAction(result =>
            {
                if (result.Response != CloudServiceResponse.Success)
                {
                    Debug.Log($"Failed to host Cloud Anchor: {result.Response}");

                    m_SceneController.OnAnchorHosted(false, result.Response.ToString());

                    return;
                }

                Debug.Log($"Cloud Anchor {result.Anchor.CloudId} was created and saved.");
                CmdSetCloudAnchorId(result.Anchor.CloudId);

                m_SceneController.OnAnchorHosted(true, result.Response.ToString());
            });
        }

        /// <summary>
        /// Resolves an anchor id and instantiates an Anchor prefab on it.
        /// </summary>
        /// <param name="cloudAnchorId">Cloud anchor id to be resolved.</param>
        private void _ResolveAnchorFromId(string cloudAnchorId)
        {
            m_SceneController.OnAnchorInstantiated(false);

            // If device is not tracking, let's wait to try to resolve the anchor.
            if (Session.Status != SessionStatus.Tracking)
            {
                return;
            }

            m_ShouldResolve = false;

            XPSession.ResolveCloudAnchor(cloudAnchorId).ThenAction(
                result =>
                {
                    if (result.Response != CloudServiceResponse.Success)
                    {
                        Debug.LogError($"Client could not resolve Cloud Anchor {cloudAnchorId}: {result.Response}");

                        m_SceneController.OnAnchorResolved(
                            false, result.Response.ToString());
                        m_ShouldResolve = true;
                        return;
                    }

                    Debug.Log($"Client successfully resolved Cloud Anchor {cloudAnchorId}.");

                    m_SceneController.OnAnchorResolved(
                        true, result.Response.ToString());
                    _OnResolved(result.Anchor.transform);
                });
        }

        /// <summary>
        /// Callback invoked once the Cloud Anchor is resolved.
        /// Callback invocado cuando el Cloud Anchor se resuelve.
        /// </summary>
        /// <param name="anchorTransform">Transform of the resolved Cloud Anchor.</param>
        private void _OnResolved(Transform anchorTransform)
        {
            // Obtiene el objeto Cloud Anchor Scene Controller y define el sistema de coordenadas para que coincida
            // con el del objeto Anchor
            var cloudAnchorController = GameObject.Find("CloudAnchorSceneController").GetComponent<SceneController>();
            cloudAnchorController.SetWorldOrigin(anchorTransform);
        }

        /// <summary>
        /// Callback invoked once the Cloud Anchor Id changes.
        /// Callback invocado cuando la Id del Cloud Anchor se modifica.
        /// </summary>
        /// <param name="newId">New identifier.</param>
        private void _OnChangeId(string newId)
        {
            if (!m_IsHost && newId != string.Empty)
            {
                m_CloudAnchorId = newId;
                m_ShouldResolve = true;
            }
        }
    }
}