using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TFMGame
{
    using GoogleARCore;
    using GoogleARCore.Examples.CloudAnchors;
    using UnityEngine;
    
#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    using Input = GoogleARCore.InstantPreviewInput;
#endif
    /// <summary>
    /// Controller for the Cloud Anchors Example. Handles the ARCore lifecycle.
    /// Controller de la escena con soporte para Cloud Anchors
    /// </summary>
    public class SceneController : MonoBehaviour
    {
        /// <summary>
        /// Controller de la UI de nuestro sistema.
        /// </summary>
        public NetworkManagerUIController UIController;

        /// <summary>
        /// GameObject que agrupa elementos de ARCore que deben de ser activados cuando se accede a la partida.
        /// </summary>
        public GameObject ARCoreRoot;

        /// <summary>
        /// The helper that will calculate the World Origin offset when performing a raycast or
        /// generating planes.
        /// Helper que calcula el World Origin cuando se realiza un raycast o se generan planos.
        /// </summary>
        public ARCoreWorldOriginHelper ARCoreWorldOriginHelper;

        /// <summary>
        /// Indicates whether the Origin of the new World Coordinate System, i.e. the Cloud Anchor,
        /// was placed.
        /// Indica si el Origen del nuevo Sistema de Coordenadas Común ha sido fijado.
        /// </summary>
        private bool m_IsOriginPlaced;

        /// <summary>
        /// Indicates whether the Anchor was already instantiated.
        /// Indica si el objeto Anchor ha sido instanciado.
        /// </summary>
        private bool m_AnchorAlreadyInstantiated;

        /// <summary>
        /// Indicates whether the Cloud Anchor finished hosting.
        /// Indica si el Cloud Anchor ha finalizado el hosting.
        /// </summary>
        private bool m_AnchorFinishedHosting;

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error,
        /// otherwise false.
        /// 
        /// </summary>
        private bool m_IsQuitting;

        /// <summary>
        /// The anchor component that defines the shared world origin.
        /// </summary>
        private Component m_WorldOriginAnchor;

        /// <summary>
        /// The last pose of the hit point from AR hit test.
        /// </summary>
        private Pose? m_LastHitPose;

        /// <summary>
        /// The current cloud anchor mode.
        /// Modo actual del cloud anchor.
        /// </summary>
        private ApplicationMode m_CurrentMode = ApplicationMode.Ready;

        /// <summary>
        /// Network Manager.
        /// </summary>
#pragma warning disable 618
        private CloudAnchorsNetworkManager m_NetworkManager;
#pragma warning restore 618

        /// <summary>
        /// Enumerates modes the example application can be in.
        /// Modos de aplicación del cloud anchor. Puede estar resolviendo el cloud anchor o en hosting.
        /// </summary>
        public enum ApplicationMode
        {
            Ready,
            Hosting,
            Resolving,
        }

        /// <summary>
        /// Prefab del board de Tracking o selección de espacio donde se fijará el board.
        /// </summary>
        public GameObject boardTrackingPrefab;

        /// <summary>
        /// Instancia del board de Tracking o selección de espacio donde se fijará el board.
        /// </summary>
        private GameObject m_BoardInstance;

        /// <summary>
        /// Procede a crear el anchor de nuestro board.
        /// </summary>
        //public Button AnchorButton;

        public GameController gameController;
        
        private TrackableHit arcoreHitResult;
        
        /// <summary>
        /// The Unity Start() method.
        /// </summary>
        public void Start()
        {
            ARCoreRoot.SetActive(false);
#pragma warning disable 618
            m_NetworkManager = UIController.GetComponent<CloudAnchorsNetworkManager>();
#pragma warning restore 618
            m_NetworkManager.OnClientConnected += _OnConnectedToServer;
            m_NetworkManager.OnClientDisconnected += _OnDisconnectedFromServer;
            
            // A Name is provided to the Game Object so it can be found by other Scripts
            // instantiated as prefabs in the scene.
            gameObject.name = "CloudAnchorSceneController";

            _ResetStatus();
        }

        /// <summary>
        /// The Unity Update() method.
        /// </summary>
        public void Update()
        {
            _UpdateApplicationLifecycle();

            // Si no se encuentra en Resolving o Hosting termina el Update.
            if (m_CurrentMode != ApplicationMode.Hosting && m_CurrentMode != ApplicationMode.Resolving)
            {
                return;
            }

            // Si se encuentra en Resolving pero el anchor no ha sido fijado todavía por quien debe fijarlo
            // termina el Update.
            if (m_CurrentMode == ApplicationMode.Resolving && !m_IsOriginPlaced)
            {
                return;
            }

            // Si el jugador no ha pulsado la pantalla se termina el Update.
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                //TrackBoard();
                return;
            }

            // Si el jugador ha pulsado un elemento de UI como un botón, se termina el Update.
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                return;
            }

            TrackableHit arcoreHitResult = new TrackableHit();
            m_LastHitPose = null;

            if (ARCoreWorldOriginHelper.Raycast(touch.position.x, touch.position.y,
                TrackableHitFlags.PlaneWithinPolygon, out arcoreHitResult))
            {
                m_LastHitPose = arcoreHitResult.Pose;
            }

            // If there was an anchor placed, then instantiate the corresponding object.
            if (m_LastHitPose != null)
            {
                if (!m_IsOriginPlaced && m_CurrentMode == ApplicationMode.Hosting)
                {
                    this.arcoreHitResult = arcoreHitResult;
                    _InstantiateTrackingBoard();
                }
            }
        }



        /// <summary>
        /// Procede a crear el anchor de nuestro board en sitio donde se había clickado previamente para colocar el board
        /// de posicionamiento o Tracking.
        /// </summary>
        public void AnchorBoard()
        {
            m_WorldOriginAnchor = arcoreHitResult.Trackable.CreateAnchor(arcoreHitResult.Pose);

            SetWorldOrigin(m_WorldOriginAnchor.transform);
            _InstantiateAnchor();
            OnAnchorInstantiated(true);

            DestroyImmediate(m_BoardInstance);
            
            UIController.ChangeAnchorButtonUIVisibility(false);
        }

        public void StartGame()
        {
            if (_CanStartGame())
            {
                gameController.StartGame();
            }
        }
        
        /// <summary>
        /// Sets the apparent world origin so that the Origin of Unity's World Coordinate System
        /// coincides with the Anchor. This function needs to be called once the Cloud Anchor is
        /// either hosted or resolved.
        /// </summary>
        /// <param name="anchorTransform">Transform of the Cloud Anchor.</param>
        public void SetWorldOrigin(Transform anchorTransform)
        {
            if (m_IsOriginPlaced)
            {
                Debug.LogWarning("The World Origin can be set only once.");
                return;
            }

            m_IsOriginPlaced = true;

            ARCoreWorldOriginHelper.SetWorldOrigin(anchorTransform);
        }

        /// <summary>
        /// Handles user intent to enter a mode where they can place an anchor to host or to exit
        /// this mode if already in it.
        /// </summary>
        public void OnEnterHostingModeClick()
        {
            if (m_CurrentMode == ApplicationMode.Hosting)
            {
                m_CurrentMode = ApplicationMode.Ready;
                _ResetStatus();
                Debug.Log("Reset ApplicationMode from Hosting to Ready.");
            }

            m_CurrentMode = ApplicationMode.Hosting;
            _SetPlatformActive();
        }

        /// <summary>
        /// Handles a user intent to enter a mode where they can input an anchor to be resolved or
        /// exit this mode if already in it.
        /// </summary>
        public void OnEnterResolvingModeClick()
        {
            if (m_CurrentMode == ApplicationMode.Resolving)
            {
                m_CurrentMode = ApplicationMode.Ready;
                _ResetStatus();
                Debug.Log("Reset ApplicationMode from Resolving to Ready.");
            }

            m_CurrentMode = ApplicationMode.Resolving;
            _SetPlatformActive();
        }

        /// <summary>
        /// Callback indicating that the Cloud Anchor was instantiated and the host request was
        /// made.
        /// </summary>
        /// <param name="isHost">Indicates whether this player is the host.</param>
        public void OnAnchorInstantiated(bool isHost)
        {
            if (m_AnchorAlreadyInstantiated)
            {
                return;
            }

            m_AnchorAlreadyInstantiated = true;
            UIController.OnAnchorInstantiated(isHost);
        }

        /// <summary>
        /// Callback indicating that the Cloud Anchor was hosted.
        /// </summary>
        /// <param name="success">If set to <c>true</c> indicates the Cloud Anchor was hosted
        /// successfully.</param>
        /// <param name="response">The response string received.</param>
        public void OnAnchorHosted(bool success, string response)
        {
            m_AnchorFinishedHosting = success;
            UIController.OnAnchorHosted(success, response);
        }

        /// <summary>
        /// Callback indicating that the Cloud Anchor was resolved.
        /// </summary>
        /// <param name="success">If set to <c>true</c> indicates the Cloud Anchor was resolved
        /// successfully.</param>
        /// <param name="response">The response string received.</param>
        public void OnAnchorResolved(bool success, string response)
        {
            UIController.OnAnchorResolved(success, response);
        }

        /// <summary>
        /// Callback that happens when the client successfully connected to the server.
        /// </summary>
        private void _OnConnectedToServer()
        {
            if (m_CurrentMode == ApplicationMode.Hosting)
            {
                UIController.ShowDebugMessage("Find a plane, tap to create a Cloud Anchor.");
                //var team = GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>().getTeam();
                //UIController.ShowDebugMessage(team);
                UIController.ChangeTeamScreenVisibility(true);
            }
            else if (m_CurrentMode == ApplicationMode.Resolving)
            {
                UIController.ShowDebugMessage("Waiting for Cloud Anchor to be hosted...");
                //var team = GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>().getTeam();
                //UIController.ShowDebugMessage(team);
                UIController.ChangeTeamScreenVisibility(true);
            }
            else
            {
                _QuitWithReason("Connected to server with neither Hosting nor Resolving mode. " +
                                "Please start the app again.");
            }
        }

        /// <summary>
        /// Callback that happens when the client disconnected from the server.
        /// </summary>
        private void _OnDisconnectedFromServer()
        {
            _QuitWithReason("Network session disconnected! " +
                            "Please start the app again and try another room.");
        }

        /// <summary>
        /// Instantiates the anchor object at the pose of the m_LastPlacedAnchor Anchor. This will
        /// host the Cloud Anchor.
        /// </summary>
        private void _InstantiateAnchor()
        {
            // The anchor will be spawned by the host, so no networking Command is needed.
            GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>()
                .SpawnAnchor(Vector3.zero, Quaternion.identity, m_WorldOriginAnchor);
        }

        private void _InstantiateTrackingBoard()
        {
            if (m_BoardInstance != null)
            {
                DestroyImmediate(m_BoardInstance);
            }

            if (m_LastHitPose != null)
            {
                Vector3 pos = m_LastHitPose.Value.position;
                Quaternion rotation = Frame.Pose.rotation;
                rotation.z = 0;
                rotation.x = 0;
                m_BoardInstance = Instantiate(boardTrackingPrefab, pos, rotation, transform);

                UIController.ChangeAnchorButtonUIVisibility(true);
            }

        }

        /// <summary>
        /// Sets the corresponding platform active.
        /// </summary>
        private void _SetPlatformActive()
        {
            ARCoreRoot.SetActive(true);
        }

        private bool _CanStartGame()
        {
            switch (m_CurrentMode)
            {
                case ApplicationMode.Resolving:
                    return m_IsOriginPlaced;
                case ApplicationMode.Hosting:
                    return m_IsOriginPlaced && m_AnchorFinishedHosting;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Resets the internal status.
        /// </summary>
        private void _ResetStatus()
        {
            // Reset internal status.
            m_CurrentMode = ApplicationMode.Ready;
            if (m_WorldOriginAnchor != null)
            {
                Destroy(m_WorldOriginAnchor.gameObject);
            }

            m_WorldOriginAnchor = null;
        }

        /// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            var sleepTimeout = SleepTimeout.NeverSleep;

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                sleepTimeout = lostTrackingSleepTimeout;
            }

            Screen.sleepTimeout = sleepTimeout;

            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _QuitWithReason("Camera permission is needed to run this application.");
            }
            else if (Session.Status.IsError())
            {
                _QuitWithReason("ARCore encountered a problem connecting. " +
                                "Please start the app again.");
            }
        }

        /// <summary>
        /// Quits the application after 5 seconds for the toast to appear.
        /// </summary>
        /// <param name="reason">The reason of quitting the application.</param>
        private void _QuitWithReason(string reason)
        {
            if (m_IsQuitting)
            {
                return;
            }

            UIController.ShowDebugMessage(reason);
            m_IsQuitting = true;
            Invoke("_DoQuit", 5.0f);
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void _DoQuit()
        {
            Application.Quit();
        }
        
        
        
        /*
         private void TrackBoard()
        {
            TrackableHit trackable;
            TrackableHitFlags raycastFilter =
                TrackableHitFlags.PlaneWithinBounds |
                TrackableHitFlags.PlaneWithinPolygon;
            if (Frame.Raycast (Frame.Pose.position.x, Frame.Pose.position.y, raycastFilter, out trackable))
            {
                var detectedPlane = trackable.Trackable as DetectedPlane;
                
                if (boardInstance != null)
                {
                    DestroyImmediate (boardInstance);
                }

                if (detectedPlane != null)
                {
                    Vector3 pos = detectedPlane.CenterPose.position;
                    Quaternion rotation = Frame.Pose.rotation;
                    rotation.z = 0;
                    rotation.x = 0;
                    boardInstance = Instantiate(boardTrackingPrefab, pos, rotation, transform);
        
                    AnchorButton.gameObject.SetActive(true);
                }
            }
        }
         */
    }
}