using System.Collections;
using UnityEngine;

namespace TFMGame
{
    /// <summary>
    /// Controlador de objetos de comida que aparecen en la escena
    /// </summary>
    public class FoodController : MonoBehaviour
    {
        private GameMode m_GameMode;
        
        public GameController m_GameController;
        
        private float foodAge;

        private void Start()
        {
            if (m_GameController != null)
            {
                m_GameController.OnVariableChange += OnGameModeChanged;
            }

            StartCoroutine(InstantiateFood());
        }

        private void OnGameModeChanged(GameMode gameMode)
        {
            m_GameMode = gameMode;
        }
        
        private IEnumerator InstantiateFood()
        {
            while (m_GameMode != GameMode.End)
            {
                yield return new WaitForSeconds(3);
                if (m_GameMode == GameMode.Playing)
                {
                    _InstantiateFood();
                }
            }
            
        }
        
        private void _InstantiateFood()
        {
            Vector3 position = new Vector3();
            position.x = Random.Range(-0.2f, 0.2f);
            position.y = 0.1f;
            position.z = Random.Range(-0.2f, 0.2f);
            GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>().CmdSpawnFood(position, Quaternion.identity);
        }

        private void Update()
        {
            
        }
    }
}