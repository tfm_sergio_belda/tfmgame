namespace TFMGame
{
    /// <summary>
    /// Estados del juego
    /// </summary>
    public enum GameMode
    {
        Begin,
        Playing,
        End
    }
}