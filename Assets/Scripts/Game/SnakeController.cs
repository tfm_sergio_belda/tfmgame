using System;
using UnityEngine;
using UnityEngine.Networking;

namespace TFMGame
{
    /// <summary>
    /// Controlador de la serpiente del juego
    /// </summary>
    public class SnakeController : MonoBehaviour
    {
        private GameMode m_GameMode;
        
        public GameController m_GameController;

        private Boolean Instantiated;
        
        private void Start()
        {
            Instantiated = false;
            if (m_GameController != null)
            {
                m_GameController.OnVariableChange += OnGameModeChanged;
            }
        }
        
        private void OnGameModeChanged(GameMode gameMode)
        {
            m_GameMode = gameMode;
            if (m_GameMode == GameMode.Playing && !Instantiated)
            {
                _InstantiateSnake();
                Instantiated = true;
            }
        }

        private void Update()
        {
            
        }
        
        private void _InstantiateSnake()
        {
            Vector3 position = Vector3.zero;
            position.y += 0.1f;
            GameObject.Find("LocalPlayer").GetComponent<LocalPlayerController>().CmdSpawnSnake(position, Quaternion.identity);
        }
    }
}