﻿using System;
using System.Collections;
using System.ComponentModel;
using GoogleARCore;

namespace TFMGame
{
    using UnityEngine;
    
    /// <summary>
    /// Controlador del juego
    /// </summary>
    public class GameController : MonoBehaviour
    {
        //public Pose? m_LastHitPose;
        
        private GameMode m_GameMode;
        
        public GameMode GameMode
        {
            get => m_GameMode;
            set
            {
                if (m_GameMode == value) return;
                m_GameMode = value;
                if (OnVariableChange != null)
                    OnVariableChange(m_GameMode);
            }
        }
        
        public event OnVariableChangeDelegate OnVariableChange;

        public delegate void OnVariableChangeDelegate(GameMode newVal);
        
        
        // Start is called before the first frame update
        private void Start()
        {
            GameMode = GameMode.Begin;
        }
    
        // Update is called once per frame
        private void Update()
        {
            
        }

        public void StartGame()
        {
            GameMode = GameMode.Playing;
        }
    }
}
