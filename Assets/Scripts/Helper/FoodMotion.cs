using UnityEngine;
using UnityEngine.Networking;

namespace TFMGame
{
    /// <summary>
    /// Animador de los obejetos de comida
    /// </summary>
    public class FoodMotion : NetworkBehaviour
    {
        /// <summary>
        /// Velocidad de giro
        /// </summary>
        public float speed = 20f;

        /// <summary>
        /// Método Update para controlar rotación de objeto continuamente
        /// </summary>
        private void Update()
        {
            // Rotación del obejeto
            transform.Rotate(Vector3.down, Time.deltaTime * speed * 5);
        }
    }
}