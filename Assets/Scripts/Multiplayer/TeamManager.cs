using System;
using UnityEngine;
using UnityEngine.Networking;

namespace TFMGame
{
    public class TeamManager : NetworkBehaviour
    {
        public static TeamManager instance;
        private static int playerCount;
        [SyncVar] private int team1Count;
        [SyncVar] private int team2Count;
        void Awake()
        {
            if(instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                DestroyImmediate(gameObject);
                return;
            }
        }

        private void Start()
        {
            gameObject.name = "TeamManager";
        }

#pragma warning disable 618
        [Command]
#pragma warning restore 618
        public void CmdSetPlayerTeam(GameObject newPlayer, Team team)
        {
            var player = newPlayer.GetComponent<LocalPlayerController>();
            if (team == Team.Kind)
            {
                if (team1Count < 2)
                {
                    player.teamNumber = team;
                    team1Count++;
                }
            } else if (team == Team.Wicked)
            {
                if (team2Count < 2)
                {
                    player.teamNumber = team;
                    team2Count++;
                }
            }
        }
    }
}