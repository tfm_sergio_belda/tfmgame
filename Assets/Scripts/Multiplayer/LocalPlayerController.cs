using System;
using System.Collections;
using UnityEngine.UI;

namespace TFMGame
{
    using UnityEngine;
    using UnityEngine.Networking;

#pragma warning disable 618
    public class LocalPlayerController : NetworkBehaviour
#pragma warning restore 618
    {
        public GameObject SnakePrefab;

        public GameObject AnchorPrefab;

        public GameObject StarPrefab;
        
        public GameObject[] FoodModels;
        
        public GameObject BoardTrackingPrefab;

        public event Action OnTeamSelected;

        //[SyncVar(hook = "OnPlayerIDChanged")] public string playerID;
        [SyncVar(hook = "OnTeamChanged")] public Team teamNumber;

        private TeamManager teamManager;

        private void Awake()
        {
        }

        private void Start()
        {
            teamManager = GameObject.Find("TeamManager").GetComponent<TeamManager>();
        }

#pragma warning disable 618
        [Command]
#pragma warning restore 618
        void CmdSetTeam(Team team)
        {
            teamManager.CmdSetPlayerTeam(gameObject, team);
        }

        public void SetTeam(Team team)
        {
            CmdSetTeam(team);
        }

        public string GetTeam()
        {
            return teamNumber.ToString();
        }


        /// <summary>
        /// The Unity OnStartLocalPlayer() method.
        /// </summary>
        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();

            // A Name is provided to the Game Object so it can be found by other Scripts, since this
            // is instantiated as a prefab in the scene.
            gameObject.name = "LocalPlayer";
            
            //CmdSetTeam(gameObject);
        }

        void OnTeamChanged(Team newTeam)
        {
            teamNumber = newTeam;
            if (OnTeamSelected != null)
            {
                OnTeamSelected();
            }
        }

        /// <summary>
        /// Will spawn the origin anchor and host the Cloud Anchor. Must be called by the host.
        /// </summary>
        /// <param name="position">Position of the object to be instantiated.</param>
        /// <param name="rotation">Rotation of the object to be instantiated.</param>
        /// <param name="anchor">The ARCore Anchor to be hosted.</param>
        public void SpawnAnchor(Vector3 position, Quaternion rotation, Component anchor)
        {
            // Instantiate Anchor model at the hit pose.
            var anchorObject = Instantiate(AnchorPrefab, position, rotation);

            // Anchor must be hosted in the device.
            anchorObject.GetComponent<BoardAnchorController>().HostLastPlacedAnchor(anchor);

            // Host can spawn directly without using a Command because the server is running in this
            // instance.
#pragma warning disable 618
            NetworkServer.Spawn(anchorObject);
#pragma warning restore 618
        }

        /// <summary>
        /// A command run on the server that will spawn the Star prefab in all clients.
        /// </summary>
        /// <param name="position">Position of the object to be instantiated.</param>
        /// <param name="rotation">Rotation of the object to be instantiated.</param>
#pragma warning disable 618
        [Command]
#pragma warning restore 618
        public void CmdSpawnStar(Vector3 position, Quaternion rotation)
        {
            // Instantiate Star model at the hit pose.
            var starObject = Instantiate(StarPrefab, position, rotation);

            // Spawn the object in all clients.
#pragma warning disable 618
            NetworkServer.Spawn(starObject);
#pragma warning restore 618
        }
        
        /// <summary>
        /// A command run on the server that will spawn the Star prefab in all clients.
        /// </summary>
        /// <param name="position">Position of the object to be instantiated.</param>
        /// <param name="rotation">Rotation of the object to be instantiated.</param>
#pragma warning disable 618
        [Command]
#pragma warning restore 618
        public void CmdSpawnSnake(Vector3 position, Quaternion rotation)
        {
            // Instantiate Star model at the hit pose.
            var snakeObject = Instantiate(SnakePrefab, position, rotation);
            
            // Spawn the object in all clients.
#pragma warning disable 618
            NetworkServer.Spawn(snakeObject);
#pragma warning restore 618
            
            //GameObject.Find("CloudAnchorSnakeController").
              //  GetComponent<CloudAnchorSnakeController>().SetInstance(snakeObject);
        }
        
        /// <summary>
        /// A command run on the server that will spawn the Star prefab in all clients.
        /// </summary>
        /// <param name="position">Position of the object to be instantiated.</param>
        /// <param name="rotation">Rotation of the object to be instantiated.</param>
#pragma warning disable 618
        [Command]
#pragma warning restore 618
        public void CmdSpawnFood(Vector3 position, Quaternion rotation)
        {
            GameObject FoodItem = FoodModels [Random.Range (0, FoodModels.Length)];
            
            // Instantiate Food model at the hit pose.
            var foodInstance = Instantiate(FoodItem, position, rotation);
            
            // Spawn the object in all clients.
#pragma warning disable 618
            NetworkServer.Spawn(foodInstance);
#pragma warning restore 618

            // Destroy the object in all clients.
#pragma warning disable 618
            Destroy(foodInstance, 2);
#pragma warning restore 618
        }
    }
}