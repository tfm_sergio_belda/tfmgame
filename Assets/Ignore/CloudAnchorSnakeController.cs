﻿using UnityEngine.AI;
using UnityEngine.Networking;

namespace TFMGame
{
    using UnityEngine;
    
    public class CloudAnchorSnakeController : MonoBehaviour
    {
        public float SnakeRadius;
        public float SnakeTimer;
 
        private Transform target;
        private NavMeshAgent agent;
        private float timer;
 
        // Use this for initialization
        void OnEnable () {
            agent = GetComponent<NavMeshAgent> ();
            timer = SnakeTimer;
        }

        private GameObject snakeInstance;
        // Start is called before the first frame update
        void Start()
        {
            GetNewPosition();
        }

        private void GetNewPosition()
        {
            /*
            var transformPosition = transform.position;
            transformPosition.x = -1f;
            */
        }

        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;
 
            if (timer >= SnakeTimer) {
                Vector3 newPos = RandomNavSphere(transform.position, SnakeRadius, -1);
                agent.SetDestination(newPos);
                timer = 0;
            }
        }

        private static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
            Vector3 randDirection = Random.insideUnitSphere * dist;
 
            randDirection += origin;
 
            NavMeshHit navHit;
 
            NavMesh.SamplePosition (randDirection, out navHit, 1.0f, NavMesh.AllAreas);
 
            return navHit.position;
        }
    }


}

