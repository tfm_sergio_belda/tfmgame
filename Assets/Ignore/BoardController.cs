﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
// NOTE:
// - InstantPreviewInput does not support `deltaPosition`.
// - InstantPreviewInput does not support input from
//   multiple simultaneous screen touches.
// - InstantPreviewInput might miss frames. A steady stream
//   of touch events across frames while holding your finger
//   on the screen is not guaranteed.
// - InstantPreviewInput does not generate Unity UI event system
//   events from device touches. Use mouse/keyboard in the editor
//   instead.
using Input = GoogleARCore.InstantPreviewInput;
#endif
public class BoardController : MonoBehaviour
{
    private DetectedPlane detectedPlane;

    private GameObject boardInstance;

    public GameObject boardPrefab;

    public GameObject boardAnchorPrefab;

    private Anchor anchor;
    
    private Boolean anchored = false;

    public Button AnchorButton;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // The tracking state must be FrameTrackingState.Tracking
        // in order to access the Frame.
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }

        if (anchored)
        {
            return;
        }

        FindPlane();

        if (boardInstance != null)
        {
            MoveBoard();
        }
    }

    private void MoveBoard()
    {
        //TODO
    }

    /// <summary>
    /// 
    /// </summary>
    private void FindPlane()
    {
        
        TrackableHit trackable;
        TrackableHitFlags raycastFilter =
            TrackableHitFlags.PlaneWithinBounds |
            TrackableHitFlags.PlaneWithinPolygon;
        if (Frame.Raycast (Frame.Pose.position.x, Frame.Pose.position.y, raycastFilter, out trackable))
        {
            SetSelectedPlane (trackable.Trackable as DetectedPlane);
        }

        /*
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds;    

        if (Frame.Raycast (Screen.width/2, Screen.height/2, raycastFilter, out hit))
        {
            Vector3 pt = hit.Pose.position;
            DetectedPlane detectedPlane = hit.Trackable as DetectedPlane;

            this.detectedPlane = detectedPlane;
            
            SpawnBoard(pt);
        }
        */
    }
    
    public void SetSelectedPlane(DetectedPlane detectedPlane)
    {
        this.detectedPlane = detectedPlane;
        SpawnBoard();
    }
    
    void SpawnBoard ()
    {
        if (boardInstance != null)
        {
            DestroyImmediate (boardInstance);
        }

        Vector3 pos = detectedPlane.CenterPose.position;
        Quaternion rotation = Frame.Pose.rotation;
        rotation.z = 0;
        rotation.x = 0;
        boardInstance = Instantiate (boardPrefab, pos,
            rotation, transform);
        
        AnchorButton.gameObject.SetActive(true);
    }
    
    
    /*
    void SpawnBoard (Vector3 pos)
    {
        if (boardInstance != null)
        {
            DestroyImmediate (boardInstance);
        }

        Quaternion rotation = Frame.Pose.rotation;
        rotation.z = 0;
        rotation.x = 0;
        boardInstance = Instantiate (boardPrefab, pos, rotation, transform);
        
        AnchorButton.gameObject.SetActive(true);
    }
    */

    public void CreateAnchor()
    {
        Instantiate(boardAnchorPrefab, boardInstance.transform.position, boardInstance.transform.rotation);
        anchored = true;
        
        DestroyImmediate (boardInstance);
        AnchorButton.gameObject.SetActive(false);
    }
}
